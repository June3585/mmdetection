# Copyright (c) OpenMMLab. All rights reserved.
import copy
import os.path as osp
from typing import List, Union

import mmcv
from mmengine.fileio import get, get_local_path, list_from_file

from mmdet.registry import DATASETS
from .base_det_dataset import BaseDetDataset


@DATASETS.register_module()
class NcnDataset(BaseDetDataset):
    METAINFO = {
        'classes': ('bicycle', 'bus', 'car', 'motorcycle', 
                    'person', 'rider', 'truck', 'other_vehicle'),
        'palette': [(220, 20, 60), (255, 0, 0), (0, 0, 142), (0, 0, 70),
                    (0, 60, 100), (0, 80, 100), (0, 0, 230), (119, 11, 32)]
    }

    def load_data_list(self) -> List[dict]:
        """Load annotation from XML style ann_file.

        Returns:
            list[dict]: Annotation info from XML file.
        """
        assert self._metainfo.get('classes', None) is not None, \
            '`classes` in `XMLDataset` can not be None.'
        self.cat2label = {
            cat: i
            for i, cat in enumerate(self._metainfo['classes'])
        }

        img_ids = list_from_file(self.ann_file, backend_args=self.backend_args)

        data_list = []
        for i, img_id in enumerate(img_ids):
            img_name = f'{img_id}.jpg'
            txt_name = f'{img_id}.txt'

            raw_img_info = {}
            raw_img_info['img_id'] = img_id
            raw_img_info['img_name'] = img_name
            raw_img_info['txt_name'] = txt_name

            parsed_data_info = self.parse_data_info({
                'raw_img_info': raw_img_info
            })
            data_list.append(parsed_data_info)

        return data_list

    def parse_data_info(self, raw_data_info: dict) -> Union[dict, List[dict]]:
        """Parse raw annotation to target format.

        Args:
            raw_data_info (dict): Raw data information load from ``ann_file``

        Returns:
            Union[dict, List[dict]]: Parsed annotation.
        """
        img_info = raw_data_info['raw_img_info']
        # ann_info = raw_data_info['raw_ann_info']

        data_info = {}

        img_path = osp.join(self.data_prefix['img_dir'], img_info['img_name'])
        txt_path = osp.join(self.data_prefix['ann_dir'], img_info['txt_name'])

        data_info['img_path'] = img_path
        data_info['img_id'] = img_info['img_id']

        with get_local_path(txt_path, backend_args=self.backend_args) as local_path:
            with open(local_path, 'r') as f:
                raw_ann_info = f.readlines()

        img_bytes = get(img_path, backend_args=self.backend_args)
        img = mmcv.imfrombytes(img_bytes, backend='cv2')
        height, width = img.shape[:2]
        del img, img_bytes

        data_info['height'] = height
        data_info['width'] = width

        data_info['instances'] = self._parse_instance_info(
            raw_ann_info, minus_one=True)

        return data_info

    def _parse_instance_info(self,
                             raw_ann_info: list,
                             minus_one: bool = True) -> List[dict]:
        """parse instance information.

        Args:
            raw_ann_info (ElementTree): ElementTree object.
            minus_one (bool): Whether to subtract 1 from the coordinates.
                Defaults to True.

        Returns:
            List[dict]: List of instances.
        """
        instances = []
        for obj in raw_ann_info:
            instance = {}
            if len(obj) < 5:
                continue
            category, xmin, ymin, xmax, ymax = obj.strip('\n').split(',')

            if category not in self._metainfo['classes']:
                continue
        
            bbox = [
                int(xmin), int(ymin), int(xmax), int(ymax)
            ]

            ignore = False
            if ignore:
                instance['ignore_flag'] = 1
            else:
                instance['ignore_flag'] = 0

            instance['bbox'] = bbox
            instance['bbox_label'] = self.cat2label[category]     
            instances.append(instance)

        return instances

    def filter_data(self) -> List[dict]:
        """Filter annotations according to filter_cfg.

        Returns:
            List[dict]: Filtered results.
        """
        if self.test_mode:
            return self.data_list

        filter_empty_gt = self.filter_cfg.get('filter_empty_gt', False) \
            if self.filter_cfg is not None else False
        min_size = self.filter_cfg.get('min_size', 0) \
            if self.filter_cfg is not None else 0

        valid_data_infos = []
        for i, data_info in enumerate(self.data_list):
            width = data_info['width']
            height = data_info['height']
            if filter_empty_gt and len(data_info['instances']) == 0:
                continue
            if min(width, height) >= min_size:
                valid_data_infos.append(data_info)

        return valid_data_infos
